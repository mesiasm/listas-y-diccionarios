"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 25:
             El virus atacó el sistema de archivos de la supercomputadora y rompió el control
             de los derechos de acceso a los archivos. Para cada archivo hay un conjunto conocido
             de operaciones que pueden aplicarse a él:
             escribe W ,
             leer R ,
             ejecutar X
             La primera línea contiene el número N , el número de archivos contenidos en el sistema
             de archivos. Las siguientes N líneas contienen los nombres de archivo y las operaciones
             permitidas con ellos, separadas por espacios. La siguiente línea contiene un número entero
             M : el número de operaciones para los archivos. En las últimas líneas M , especifique las
             operaciones que se solicitan para los archivos. Se puede solicitar un archivo muchas veces.
             Debe recuperar el control sobre los derechos de acceso a los archivos. Para cada solicitud,
             su programa debe devolver OK si la operación solicitada es válida o Acceso denegado si la
             operación no es válida."""
# Read a string:
# s = input()
# Print a value:
# print(s)
N = int(input())
palabras = {}
for i in range(N):
    lista = list(input().split())
    palabras[lista[0]] = lista[1:len(lista)]
M = int(input())
for t in range(M):
    a = list(input().split())
    if 'W' in palabras[a[1]] and a[0] == 'write':
        print('OK')
    elif 'R' in palabras[a[1]] and a[0] == 'read':
        print('OK')
    elif 'X' in palabras[a[1]] and a[0] == 'execute':
        print('OK')
    else:
        print('Access denied')
"""
OTRA FORMA DE RESOLVER:
ACTION_TO_RIGHT = {
  'read': 'R',
  'write': 'W',
  'execute': 'X',
}

file_rights = {}
for _ in range(int(input())):
  filename, *rights = input().split()
  file_rights[filename] = set(rights)
for _ in range(int(input())):
  action, filename = input().split()
  if ACTION_TO_RIGHT[action] in file_rights[filename]:
    print('OK')
  else:
    print('Access denied')"""
