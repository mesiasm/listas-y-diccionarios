"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 21:
            El texto se da en una sola línea. Para cada palabra del texto, cuente el número
            de veces que aparece antes."""
# Read a string:
# s = input()
# Print a value:
# print(s)
texto = input().split()
frecuencia = {}
for palabra in texto:
  if palabra not in frecuencia:
    frecuencia[palabra] = 0
  print(frecuencia[palabra], end=' ')
  frecuencia[palabra] += 1