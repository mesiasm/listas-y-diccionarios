"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 23:
            La primera línea contiene el número de registros. Después de eso,
            cada entrada contiene el nombre del candidato y el número de votos
            que obtuvieron en algún estado. Cuente los resultados de las elecciones:
            sume el número de votos para cada candidato. Imprimir candidatos en
            orden alfabético."""
# Read a string:
# s = input()
# Print a value:
# print(s)
num_votos = {}
for _ in range(int(input())):
    candidato, votos = input().split()
    num_votos[candidato] = num_votos.get(candidato, 0) + int(votos)

for candidato, votos in sorted(num_votos.items()):
    print(candidato, votos)
